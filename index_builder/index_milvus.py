from feature_extractror import features_dict
from pymilvus import (
    connections,
    utility,
    FieldSchema, CollectionSchema, DataType,
    Collection,
)


# подключение и удаление существующей коллекции.
connections.connect("default", host="localhost", port="19530")
utility.drop_collection("image_vectors")

# создание коллекции
fields = [
    FieldSchema(name="name", dtype=DataType.VARCHAR,
                is_primary=True, auto_id=False, max_length=100),
    FieldSchema(name="vector", dtype=DataType.FLOAT_VECTOR, dim=2048)
]

schema = CollectionSchema(
    fields, "Image Vectors")

image_vectors = Collection("image_vectors", schema, consistency_level="Strong")


# запись векторов
names = []
vectors = []
for key, val in features_dict.items():
    names.append(key)
    vectors.append(val)

insert_result = image_vectors.insert([names, vectors])

image_vectors.flush()

# создание поискового индекса (оптимизация)
index = {
    "index_type": "IVF_FLAT",
    "metric_type": "L2",
    "params": {"nlist": 128},
}

image_vectors.create_index("vector", index)

# количество элементов в коллекции
print(f"Количество записей в коллекции: {image_vectors.num_entities}")
