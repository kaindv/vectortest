import nmslib
import os
from feature_extractror import images_path
from feature_extractror import features_dict
import numpy as np


class NMSLIBIndex():
    def __init__(self, vectors, labels):
        self.dimention = vectors.shape[1]
        self.vectors = vectors.astype('float32')
        self.labels = labels

    def build(self):
        self.index = nmslib.init(method='hnsw', space='cosinesimil')
        self.index.addDataPointBatch(self.vectors)
        self.index.createIndex({'post': 2})
        self.index.saveIndex('index.idx', save_data = True)


data = dict()
vector_array = list()
name_array = list()
for key, val in features_dict.items():
    vector_array.append(val)
    name_array.append(key)

data['vector'] = np.array(vector_array)
data['name'] = np.array(name_array)

index = NMSLIBIndex(data["vector"], data["name"])
index.build()


