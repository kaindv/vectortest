# Vectortest
## _Приложение поиска схожих изображений_

В данной версии реализованы два эндпоинта

- /add-images   - создание коллекции векторов
- /search       - поисх похожего изображения

## Стэк

- [Python]  - Cpython build 3.10.x
- [Fastapi] - 0.104.1
- [Milvus]  - v2.4.0-rc
- [Docker]

## Установка

Установить Docker Engine

Склонировать репозиторий. Затем установить весь бэкенд:

```sh
sudo docker compose up -d --build
```


СДЕЛАТЬ ИНДЕКС (коллекцию) В БАЗУ:
Пример с загрузкой мультиформы с изображениями (здесь - 3 штуки).
```sh
curl -X POST -H 'accept: application/json' -H 'Content-Type: multipart/form-data' -F files=@/home/kain/DEV/vectortest/images_store/6389.jpg -F files=@/home/kain/DEV/vectortest/images_store/6371.jpg -F files=@/home/kain/DEV/vectortest/images_store/6377.jpg  http://localhost:8000/add-images
```

Ответ:
{"collection":"collection_627318","entities":3}


ПОИСК ПОХОЖЕГО:
Здесь помимо файла передаётся имя коллекции.
```sh
curl -X POST -F file=@/home/kain/DEV/vectortest/images_store/6342.jpg -F "collection=collection_627318" http://localhost:8000/search
```

Ответ:
{"nearest_image":"6342.jpg","distance":"0.0"}