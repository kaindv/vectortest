# Используем базовый образ python
FROM python:3.10

# Устанавливаем рабочую директорию в контейнере
WORKDIR /app

# Копируем файл зависимостей в контейнер
COPY requirements.txt .

# Обновляем pip
RUN pip install --upgrade pip

# Устанавливаем зависимости приложения
RUN pip install -r requirements.txt

# Копируем исходный код приложения в контейнер
COPY ./app .

# Запускаем приложение при старте контейнера
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
