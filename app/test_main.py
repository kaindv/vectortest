import io
from fastapi.testclient import TestClient

from main import app


client = TestClient(app)


def test_upload_image():
    with open("../images_store/6391.jpg", "rb") as image_file:
        image_data = io.BytesIO(image_file.read())

    response = client.post(
        "/find-similar", files={"image": ("image.jpg", image_data, "image/jpeg")})
    assert response.status_code == 200
