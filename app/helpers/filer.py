from fastapi.exceptions import HTTPException


async def file_check(file) -> bool:
    """
    Проверка валидности загружаемого файла.
    Размер не более 5 Мб
    Типы данных ["image/jpeg", "image/png", "image/gif"]

    :param file: файл из формы.
    """
    # Проверка размера (не более 6 Мб)
    file.file.seek(0, 2)
    file_size = file.file.tell()

    # перенос курсора обратно на 0
    await file.seek(0)

    if file_size > 6 * 1024 * 1024:
        # более 6 Мб
        raise HTTPException(status_code=400, detail="File too large")

    # Проверка контента (MIME type)
    content_type = file.content_type
    if content_type not in ["image/jpeg", "image/png", "image/gif"]:
        raise HTTPException(status_code=400, detail="Invalid file type")

    return True
