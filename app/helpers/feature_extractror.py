import os
import numpy as np
from typing import Dict
from keras.models import Model
from keras.applications import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.preprocessing import image
from PIL import UnidentifiedImageError


IMAGE_SIZE = (224, 224)
IMAGE_DIMS = (IMAGE_SIZE[0], IMAGE_SIZE[1], 3)


class FeatureExtractor:
    def __init__(self, model: Model = ResNet50(weights='imagenet', include_top=False, pooling='avg')) -> None:
        """
        Инициализация FeatureExtractor с предобученной моделью.

        :param model: Предобученная модель Keras, используемая для извлечения признаков.
        """
        self.model = model

    def load_image(self, img_path: str) -> np.ndarray:
        """
        Загрузка и предобработка изображения из файловой системы.

        :param img_path: Путь к файлу изображения.
        :return: Предобработанное изображение, подходящее для модели.
        """
        img = image.load_img(img_path, target_size=IMAGE_SIZE)
        img_array = image.img_to_array(img)
        img_array = np.expand_dims(img_array, axis=0)
        img_array = preprocess_input(img_array)
        return img_array

    def extract_features(self, image_path: str) -> np.ndarray:
        """
        Извлечение признаков из одного изображения.

        :param image_path: Путь к файлу изображения.
        :return: Вектор признаков, извлеченных из изображения.
        """
        img_array = self.load_image(image_path)
        features = self.model.predict(img_array)
        return features.flatten()

    def extract_features_from_batch(self, directory_path: str, batch: list) -> Dict[str, np.ndarray]:
        """
        Извлечение признаков из всех изображений в директории.

        :param directory_path: Путь к директории, содержащей изображения.
        :return: Словарь с именами файлов изображений в качестве ключей и векторами признаков в качестве значений.
        """
        features_dict: Dict[str, np.ndarray] = {}
        for img in batch:
            # img_path = os.path.join(directory_path, img_name)
            try:
                features = self.extract_features(img[1])
                features_dict[img[0]] = features
            except UnidentifiedImageError as e:
                continue
        return features_dict

    def extract_uploaded_features(self, img) -> np.ndarray:
        """
        Извлечение признаков из загружаемого изображения.

        :param img: Файл изображения.
        :return: Вектор признаков, извлеченных из изображения.
        """
        # Подгон размеров
        pil_image = img.resize(IMAGE_SIZE)

        # # Если есть альфа, убирает
        if pil_image.mode == 'RGBA':
            pil_image = pil_image.convert('RGB')

        # Конвертирование в массив numpy
        numpy_image = np.array(pil_image).reshape(IMAGE_DIMS)
        numpy_image = np.expand_dims(numpy_image, axis=0)

        # получение вектора
        features = self.model.predict(numpy_image)

        return features.flatten()


feature_extractor = FeatureExtractor()
