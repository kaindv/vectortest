from helpers.feature_extractror import feature_extractor
import time
from pymilvus import (
    connections,
    # utility,
    # FieldSchema, CollectionSchema, DataType,
    Collection,
)


class SearchFeatures:
    def __init__(self, collection: str) -> None:
        # connections.connect("default", host="localhost", port="19530")
        self.collection = collection

    def search_feature(self, image_path):

        search_result = list()
        vector = feature_extractor.extract_features(image_path)

        # поиск похожего вектора
        vectors_to_search = [vector]
        search_params = {
            "metric_type": "L2",
            "params": {"nlist": 128},
        }

        image_vectors = Collection(self.collection)
        image_vectors.load()

        start_time = time.time()
        result = image_vectors.search(
            vectors_to_search, "vector", search_params, limit=1, output_fields=["name"])
        end_time = time.time()

        for hits in result:
            for hit in hits:
                search_result.append(hit)

        return search_result[0].get('name'), search_result[0].distance


# if __name__ == "__main__":
#     search_features = SearchFeatures("image_vectors")
#     res = search_features.search_feature("./images_store/6389.jpg")
#     print(res)
