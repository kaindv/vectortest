from helpers.feature_extractror import feature_extractor
import os
from pymilvus import (
    connections,
    FieldSchema, CollectionSchema, DataType,
    Collection,
)


class FeaturesCRUD:

    def __init__(self, collection) -> None:
        self.collection = collection
        # self.client = connections.connect(
        #     "default", host="localhost", port="19530")

    def create_collection(self, images_path, batch_size=1):
        """
        Создание коллекции.

        :param images_path: Путь к каталогу с изображениями.
        :param batch_size: Размер батча. Если не указан или =1, берет весь каталог сразу 
        """
        dirlist = os.listdir(images_path)
        # список списков [имя файла, путь к файлу]
        batch_all = [[name, os.path.join(images_path, name)] for name in dirlist if os.path.isfile(
            os.path.join(images_path, name))]
        features_dict = dict()

        if batch_size > 1:

            batch_all_len = len(batch_all)
            iterations_count = batch_all_len // batch_size

            for i in range(iterations_count):

                print(f'Батч № {i + 1}')

                batch = batch_all[:batch_size]

                features_batch_dict = feature_extractor.extract_features_from_batch(
                    images_path, batch)
                features_dict.update(features_batch_dict)

                del batch_all[:batch_size]

        # досылаем остаток или всю коллекцию (избежать лишних вычислений в цикле)
        features_batch_dict = feature_extractor.extract_features_from_batch(
            images_path, batch_all)
        features_dict.update(features_batch_dict)

        # создание коллекции
        fields = [
            FieldSchema(name="id", dtype=DataType.INT64,
                        is_primary=True, auto_id=True),
            FieldSchema(name="name", dtype=DataType.VARCHAR,
                        is_primary=False, auto_id=False, max_length=100),
            FieldSchema(name="vector", dtype=DataType.FLOAT_VECTOR, dim=2048)
        ]

        schema = CollectionSchema(
            fields, "")

        image_vectors = Collection(
            self.collection, schema, consistency_level="Strong")

        # запись векторов
        names = []
        vectors = []
        for key, val in features_dict.items():
            names.append(key)
            vectors.append(val)

        insert_result = image_vectors.insert([names, vectors])

        image_vectors.flush()

        # создание поискового индекса (оптимизация)
        index = {
            "index_type": "IVF_FLAT",
            "metric_type": "L2",
            "params": {"nlist": 128},
        }

        image_vectors.create_index("vector", index)

        # количество элементов в
        self.numentities = image_vectors.num_entities
        return {"collection": self.collection, "entities": self.numentities}

    def drop_collection(self):
        """
        Удаление коллекции.

        """
        try:
            Collection(self.collection).drop()
        except Exception as e:
            return e

        return f"Коллекция {self.collection} удалена"

    def delete_vectors(self, vectors: list):
        """
        Удаление векторов.

        :param vectors: Список id векторов.

        """
        try:
            image_vectors = Collection(self.collection)
            expr = f'id in {str(vectors)}'
            image_vectors.delete(expr)
        except Exception as e:
            return e

        return f"Векторы удалены."


# if __name__ == "__main__":
    # fcrud = FeaturesCRUD(collection="image_vectors")
    # cnt = fcrud.drop_collection()
    # cnt = fcrud.create_collection(images_path="./images_store", batch_size=12)
    # cnt = fcrud.create_collection(images_path="./images_store")
    # cnt = fcrud.delete_vectors(
    #     vectors=[448999378691729134, 448999378691729135, 448999378691729137])
    # print(cnt)
