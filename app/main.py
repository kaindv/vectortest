import os
import shutil
from tempfile import TemporaryDirectory
from random import randrange

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI, UploadFile, File, HTTPException, Form
from starlette.responses import JSONResponse
from helpers.searcher import SearchFeatures
from helpers.filer import file_check
from helpers.cruds import FeaturesCRUD
from pymilvus import connections

load_dotenv()
# Инициализация FastAPI app
app = FastAPI()

TEST_IMAGES_STORE = os.getenv('TEST_IMAGES_STORE', '/tmp/test_images/')
IMAGES_PATH = '/tmp/images_store'
BASE_IMAGES_PATH = 'test_images/original'

BATCH_SIZE = 1


@app.on_event("startup")
async def load_index():
    connections.connect("default", host="milvus-standalone", port="19530")


# Эндпоинт для поиска похожего изображения
@app.post("/search")
async def search_image(collection: str = Form(...), file: UploadFile = File(...)):
    """
    Эндпоинт принимает файл, проверяет на соответствие типу "изображение",
    возвращает название похожего изображения или ошибку.

    :param file: Файл
    :param collection: Имя коллекции
    :return: Имя файла похожего изображения и дистанция
    """

    if not collection:
        raise HTTPException(
            status_code=400, detail="Collection name is required")

    await file_check(file)
    # Временное схранение изображения-оригинала
    filename = file.filename
    temp_file_path = filename if TEST_IMAGES_STORE not in filename else filename.replace(
        TEST_IMAGES_STORE, '')

    with open(temp_file_path, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    # Поиск похожего на оригинал
    try:
        searcher = SearchFeatures(collection)
        nearest_image, distance = searcher.search_feature(temp_file_path)
    except Exception as e:
        os.remove(temp_file_path)
        return e

    # Удаление оригинала
    os.remove(temp_file_path)

    if nearest_image:
        return {"nearest_image": nearest_image, "distance": str(distance)}
    else:
        return {"message": "No similar image found"}


@app.post("/add-images")
async def add_images(files: list[UploadFile] = File(...)):
    """
    Эндпоинт принимает файлы, проверяет на соответствие типу "изображение",
    записывает векторы изображений в коллекцию.

    :param files: Файлы
    :return: Имя коллекции и количество записей в ней.
    """
    if not os.path.exists(IMAGES_PATH):
        os.makedirs(IMAGES_PATH)

    with TemporaryDirectory() as temp_dir:
        print(f"Created temporary directory: {temp_dir}")

        for file in files:
            temp_file_path = os.path.join(temp_dir, file.filename)
            print('temp_file_path', temp_file_path)
            permanent_file_path = os.path.join(IMAGES_PATH, file.filename)
            print('permanent_file_path', permanent_file_path)

            # Сохранение во временную папку
            with open(temp_file_path, 'wb') as temp_buffer:
                shutil.copyfileobj(file.file, temp_buffer)

            # # Сохранение в постоянную папку
            # with open(permanent_file_path, 'wb') as perm_buffer:
            #     file.file.seek(0)  # Сбросить указатель
            #     shutil.copyfileobj(file.file, perm_buffer)

        print(os.listdir(temp_dir))

        try:
            collection_random_name = "collection_" + \
                str(randrange(100000, 900000))
            fcrud = FeaturesCRUD(collection=str(collection_random_name))
            result = fcrud.create_collection(
                images_path=temp_dir, batch_size=BATCH_SIZE)
        except Exception as e:
            result = e

    return result


# @app.get("/init-images")
# async def init_images():
#     image_indexer.add_new_images(BASE_IMAGES_PATH)

#     # Ensure destination folder exists
#     os.makedirs(IMAGES_PATH, exist_ok=True)

#     filenames = os.listdir(BASE_IMAGES_PATH)

#     # Copy all files from the source folder to the destination folder
#     for filename in filenames:
#         source_file = os.path.join(BASE_IMAGES_PATH, filename)
#         destination_file = os.path.join(IMAGES_PATH, filename)

#         if os.path.isfile(source_file):
#             shutil.copy2(source_file, destination_file)

#     return JSONResponse(content={"message": f"Processed {len(filenames)} images."})


# Run the application
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
